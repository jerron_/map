/*

This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike
4.0 International License, by Yong Bakos.

*/

import UIKit

import MapKit

class ViewController: UIViewController, MKMapViewDelegate {
    
    
    //IBAction func openMapsAppWithURL(sender: UIButton) {
    // if let url = NSURL(string: "http://maps.apple.com/?q=Yosemite") {
    //  let app = UIApplication.sharedApplication()
    //    app.openURL(url)
    //  }
    //}
    
    @IBOutlet weak var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        super.viewDidLoad()
        mapView.setUserTrackingMode(.Follow, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func mapView(mapView: MKMapView, didUpdateUserLocation userLocation:   MKUserLocation) {
        let center = CLLocationCoordinate2D(latitude:
            userLocation.coordinate.latitude,
            longitude: userLocation.coordinate.longitude)
        let width = 1000.0 // meters
        let height = 1000.0
        let region = MKCoordinateRegionMakeWithDistance(center, width,
            height)
        mapView.setRegion(region, animated: true)
    }
    
}